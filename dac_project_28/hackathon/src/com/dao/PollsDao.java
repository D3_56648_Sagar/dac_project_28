package com.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Scanner;
import java.sql.ResultSet;

import com.pojos.Polls;
import com.pojos.Users;
import static com.utils.DBUtils.fetchConnection;
public class PollsDao
{
	private Connection cn;
	private PreparedStatement pst1,pst2,pst3,pst4;
	 Scanner sc=new Scanner(System.in);
	public PollsDao() 
	{
		try
		{
		cn=fetchConnection();
		pst1=cn.prepareStatement("SELECT * from polls where created_by=?");
	    pst2=cn.prepareStatement(" INSERT INTO polls ( title, start_datetime,end_datetime,created_by) VALUES (?, ?,?,?)");
		pst3=cn.prepareStatement(" UPDATE polls SET title=?, start_datetime=?, end_datetime=? WHERE id=?;");
		//pst4=cn.prepareStatement(" UPDATE users SET password1 = ? WHERE id =?");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	//list my pools
	 public  ArrayList<Polls> getList(int id)
	 {
		 System.out.println(id);
		 try {
	     System.out.println("Yours polls are :");	
		 ArrayList<Polls> polls =new ArrayList<Polls>();
		 pst1.setInt(1,id);
		 try(ResultSet rst = pst1.executeQuery()) {
				while (rst.next())
					polls.add(new Polls(rst.getInt(1), rst.getString(2),  rst.getString(3),  rst.getString(4), rst.getInt(5)));
	        for (Polls u : polls) {
	        	System.out.println(u);
			}
		      	
		 }
			 
			catch (Exception e) {
			// TODO: handle exception
		}
		
		 }
		 
			catch (Exception e) {
			// TODO: handle exception
		}
		 return null;
	 }
	 
	 
	 //functiom to create new poll
	 public void createPoll(int uid)
	 {
		 System.out.println(uid);
		 try {
		 
		 Scanner sc=new Scanner(System.in);
		 System.out.println("enter title : ");
		 String title=sc.next(); 
		 System.out.println("enter start date time : ");
		 String sdate=sc.next(); 
		 System.out.println("enter  end date time : ");
		 String edate=sc.next(); 
		 
		 pst2.setString(1,title);
		 pst2.setDate(2, Date.valueOf(sdate));
		 pst2.setDate(3, Date.valueOf(edate));
		 pst2.setInt(4,uid);
		 int count=pst2.executeUpdate();
		 if(count==1)
			 System.out.println("poll added");
		 else
			 System.out.println("fail to add");
		 }
			catch(Exception e)
			{
				e.printStackTrace();
			}
		 int cont=1;
		 while(cont==1)
		 {
		 System.out.println("press 1 if you want to add question in given pole");
		 cont=sc.nextInt();
		 if(cont==1)
		 {
			 
			cont=0; 
		 }
	 }
	 }
	 
	 public void EditPoll(int id) {
			System.out.println(id);
			if(id!=0)
			{
			 try {
			 
			 Scanner sc=new Scanner(System.in);
			 System.out.println("enter new title : ");
			 String title=sc.next(); 
			 System.out.println("enter new start_datetime : ");
			 String start_datetime=sc.next();  
			 sc.nextLine();
			 System.out.println("enter new end_datetime : ");
			 String end_datetime=sc.next(); 
			
			 pst3.setString(1,title);
			 pst3.setString(2,start_datetime);
			 pst3.setString(3,end_datetime);
			 pst3.setInt(4,id);
			 
			 
			 int count=pst3.executeUpdate();
			 if(count==1)
				 System.out.println("polls Updated");
			 else
				 System.out.println("fail to update");
			 }
				catch(Exception e)
				{
					e.printStackTrace();
				}	 
		 }else
			 System.out.println("login firsr before edit");
			}
		
		}
	
   

