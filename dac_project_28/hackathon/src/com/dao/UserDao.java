package com.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Scanner;
import java.sql.ResultSet;
import com.pojos.Users;
import static com.utils.DBUtils.fetchConnection;

public class UserDao
{
	private Connection cn;
	private PreparedStatement pst1,pst2,pst3,pst4;
	 Scanner sc=new Scanner(System.in);
	public UserDao() 
	{
		try
		{
		cn=fetchConnection();
		pst1=cn.prepareStatement("INSERT INTO users ( name,email, password1,mobile,gender,address,birth_date) VALUES (?,?,?,?,?,?,?)");
		pst2=cn.prepareStatement(" select * from users");
		pst3=cn.prepareStatement("UPDATE users SET name = ?, email =?, mobile = ? , address = ?,birth_date = ? WHERE id=?");
		pst4=cn.prepareStatement(" UPDATE users SET password1 = ? WHERE id =?");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void changePassword(int id)
	 {
		 try {
		 
 
		 System.out.println("please enter new password : ");
		 String pass=sc.next(); 
		 pst4.setString(1,pass);
		 pst4.setInt(2,id);
		 
		 int count=pst3.executeUpdate();
		 if(count==1)
			 System.out.println("user Updated");
		 else
			 System.out.println("fail to update");
		 }
			catch(Exception e)
			{
				e.printStackTrace();
			}
		 
		 
		 
	 }
	
	// edit
	public void Edit(int id)
	 {
		System.out.println(id);
		if(id!=0)
		{
		 try {
		 
		 Scanner sc=new Scanner(System.in);
		 System.out.println("enter new name : ");
		 String name=sc.next(); 
		 System.out.println("enter new email : ");
		 String email=sc.next();  
		 System.out.println("enter new mobile : ");
		 int mno=sc.nextInt(); 
		 System.out.println("enter new address : ");
		 String addr=sc.next(); 
		 System.out.println("enter new birthdate  date : ");
		 String date=sc.next(); 
		 pst3.setString(1,name);
		 pst3.setString(2,email);
		 pst3.setInt(3,mno);
		 pst3.setString(4,addr);
		 pst3.setDate(5,Date.valueOf(date));
		 pst3.setInt(6,id);
		 int count=pst3.executeUpdate();
		 if(count==1)
			 System.out.println("user Updated");
		 else
			 System.out.println("fail to update");
		 }
			catch(Exception e)
			{
				e.printStackTrace();
			}
		 
		 
		 
	 }else
		 System.out.println("login firsr before edit");
		}
	//for signup
 public void signup()
 {
	 try {
	 
	 Scanner sc=new Scanner(System.in);
	 System.out.println("enter name : ");
	 String name=sc.next(); 
	 System.out.println("enter email : ");
	 String email=sc.next(); 
	 System.out.println("enter password : ");
	 String pass=sc.next(); 
	 System.out.println("enter mobile : ");
	 String mno=sc.next(); 
	 System.out.println("enter gender (m/f): ");
	 String sex=sc.next(); 
	 System.out.println("enter address : ");
	 String addr=sc.next(); 
	 System.out.println("enter date : ");
	 String date=sc.next(); 
	 pst1.setString(1,name);
	 pst1.setString(2,email);
	 pst1.setString(3,pass);
	 pst1.setString(4,mno);
	 pst1.setString(5,sex);
	 pst1.setString(6,addr);
		pst1.setDate(7, Date.valueOf(date));
	 int count=pst1.executeUpdate();
	 if(count==1)
		 System.out.println("user added");
	 else
		 System.out.println("fail to add");
	 }
		catch(Exception e)
		{
			e.printStackTrace();
		}
	 
	 
	 
 }
 public  ArrayList<Users> getList()
 {
     //System.out.println("get users");	
	 ArrayList<Users> users= new ArrayList<Users>();
	 try(ResultSet rst = pst2.executeQuery()) {
			while (rst.next())
				users.add(new Users(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7), rst.getDate(8)));
       // for (Users users2 : users) {
        //	System.out.println(users2);
		//}
	      // System.out.println("get users");	
	 }
		 
		catch (Exception e) {
		// TODO: handle exception
	}
	 return users;
 }
 
 
 
 public int login(String email,String pass)
 {
	 ArrayList<Users> users= new ArrayList<Users>();
	 users=getList();
	 for (Users u : users) 
	 {
     	if(u.getEmail().equalsIgnoreCase(email)&&u.getPassword().equalsIgnoreCase(pass)) 
     	{
     		System.out.println("login sucess");
     	   return u.getId();	
		}
	 
     }
	 return 0;
  }
}